from paste.script.serve import ServeCommand
import os

ini_file = os.path.join(os.path.dirname(__file__), '.', '', 'development.ini')
ServeCommand("serve").run([ini_file])
