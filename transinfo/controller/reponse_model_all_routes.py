__author__ = 'ssingh'
import collections


class RouteStop:
    def __init__(self, routes_list, stop_id):
        self.routes_list = routes_list
        self.stop_id = stop_id

    def get_route_dict(self):
        ret_dict = collections.OrderedDict()
        ret_dict["current_routes"] = self.stop_id
        ret_dict["stop_id"] = self.routes_list
        ret_dict["error"] = False
        return ret_dict
