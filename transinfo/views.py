import datetime
from pyramid.view import view_config

from util.transit_comm import *

from transinfo.util import transinfo_response
from model.api import get_closest_stop, get_stop_by_address, get_schedule, get_all_routes, get_schedule_by_stop


@view_config(route_name='by_stop_code', request_method='GET')
def get_by_stop_code(request):
    stop_code = request.matchdict['stop_code']
    return transinfo_response.get_json_response(get_routes(stop_code))


@view_config(route_name='by_stop_code_and_scheduled', request_method='GET')
def get_by_stop_code_and_scheduled(request):
    stop_code = request.matchdict['stop_code']
    results = {}
    results["real_time"] = get_routes(stop_code)
    params = request.params.mixed()
    start_hour = params['start_hour']
    start_min = params['start_min']
    year = params['year']
    month = params['month']
    day = params['day']
    results["scheduled"] = get_schedule_by_stop(stop_code, start_hour, start_min, year, month, day, "")
    return transinfo_response.get_json_response(results)


@view_config(route_name='nearest', request_method='GET')
def nearest(request):
    params = request.params.mixed()
    latitude = params['lat']
    longitude = params['lon']
    stops = get_closest_stop(latitude, longitude)
    if stops is None or len(stops) == 0:
        return transinfo_response.get_json_response({"message": "No stop located!", "error": True})
    stop_code = stops[0].get("stop_code")
    return transinfo_response.get_json_response(get_routes(stop_code))


@view_config(route_name='nearest_and_scheduled', request_method='GET')
def nearest_and_scheduled(request):
    params = request.params.mixed()
    latitude = params['lat']
    longitude = params['lon']
    stops = get_closest_stop(latitude, longitude)
    if stops is None or len(stops) == 0:
        return transinfo_response.get_json_response({"message": "No stop located!", "error": True})
    stop_code = stops[0].get("stop_code")
    stop_name = stops[0].get("stop_name")
    params = request.params.mixed()
    start_hour = params['start_hour']
    start_min = params['start_min']
    year = params['year']
    month = params['month']
    day = params['day']
    results = {"scheduled": get_schedule_by_stop(stop_code, start_hour, start_min, year, month, day, ""),
               "real_time": get_routes(stop_code), "stop_name": stop_name, "stop_code": stop_code}
    return transinfo_response.get_json_response(results)


@view_config(route_name='schedule', request_method='GET')
def get_schedule_at_stop(request):
    params = request.params.mixed()
    latitude = params['lat']
    longitude = params['lon']
    start_hour = params['start_hour']
    start_min = params['start_min']

    if "route_code" in params:
        route_code = params['route_code']
    else:
        route_code = None

    if "year" in params:
        year = params['year']
        month = params['month']
        day = params['day']
    else:
        now = datetime.datetime.now()
        year = now.year
        month = now.month
        day = now.day

    address_or_id = params['address_or_id']
    schedules = get_schedule(latitude, longitude, start_hour, start_min, year, month, day, route_code, address_or_id)
    schedule_list = []
    for schedule in schedules:
        route_details = collections.OrderedDict()
        route_details["stop_code"] = schedule.get("stop_code")
        route_details["route"] = schedule.get("route")
        route_details["arrival_time"] = schedule.get("arrival_time")
        route_details["headsign"] = schedule.get("headsign")
        schedule_list.append(route_details)

    return transinfo_response.get_json_response(schedule_list)


@view_config(route_name='by_address', request_method='GET')
def address(request):
    params = request.params.mixed()
    address_value = params['address']
    stops = get_stop_by_address(address_value)
    if stops is None or len(stops) == 0:
        return transinfo_response.get_json_response({"message": "No stop located!", "error": True})
    stop_code = stops[0].get("stop_code")
    return transinfo_response.get_json_response(get_routes(stop_code))


@view_config(route_name='by_address_and_scheduled', request_method='GET')
def address_and_scheduled(request):
    params = request.params.mixed()
    address_value = params['address']
    stops = get_stop_by_address(address_value)
    if stops is None or len(stops) == 0:
        return transinfo_response.get_json_response({"message": "No stop located!", "error": True})
    stop_code = stops[0].get("stop_code")
    params = request.params.mixed()
    start_hour = params['start_hour']
    start_min = params['start_min']
    year = params['year']
    month = params['month']
    day = params['day']
    results = {"real_time": get_routes(stop_code),
               "scheduled": get_schedule_by_stop(stop_code, start_hour, start_min, year, month, day, "")}
    return transinfo_response.get_json_response(results)


@view_config(route_name='nearest_dummy', request_method='GET')
def nearest_dummy(request):
    return transinfo_response.get_json_response(get_routes_dummy())


@view_config(route_name='all_routes', request_method='GET')
def all_routes(request):
    return transinfo_response.get_json_response(get_all_routes())


@view_config(route_name='alerts', request_method='GET')
def alerts(request):
    return transinfo_response.get_json_response(get_alerts())