import collections
from transinfo.model.alert_model import Alert

__author__ = 'ssingh'

import xmltodict
import requests

from transinfo.model.route_model import Route
from transinfo.controller.reponse_model_all_routes import RouteStop


def get_routes(stop_code):
    root = requests.get('http://mybusnow.njtransit.com/bustime/eta/getStopPredictionsETA.jsp?route=all&stop='+stop_code)
    tree = xmltodict.parse(root.text)
    routes_list = []
    if tree.get("stop").get("noPredictionMessage") is not None:
        return {"message": tree.get("stop").get("noPredictionMessage"), "error": True, "stop_code": stop_code}

    for routes in tree.get("stop").get("pre"):
        if type(routes) is unicode:
            routes_list.append(get_from_single(tree).get_bus_details())
            break
        routes_list.append(Route(routes.get("rn"), routes.get("pt"), routes.get("pu"), routes.get("fd")).get_bus_details())
    stop_routes_dict = RouteStop(stop_code, routes_list).get_route_dict()
    return stop_routes_dict


def get_from_single(tree):
    rec = tree.get("stop").get("pre")
    return Route(rec.get("rn"), rec.get("pt"),
                 rec.get("pu"), rec.get("fd"))


def get_alerts():
    root = requests.get('http://www.njtransit.com/rss/BusAdvisories_feed.xml')
    tree = xmltodict.parse(root.text)
    alerts_list = []

    for alert in tree.get("rss").get("channel").get("item"):
        split_title = alert.get("title").split(" - ")
        alerts_list.append(Alert(split_title[0], split_title[1], alert.get("description")).get_alerts_list())
    ret_dict = collections.OrderedDict()
    ret_dict["current_alerts"] = alerts_list
    ret_dict["error"] = False
    return ret_dict


def get_routes_dummy():
    routes_list = [Route("160", "10", "MINUTES", "Test Location1").get_bus_details(),
                   Route("703", "15", "MINUTES", "Test Location2").get_bus_details()]
    stop_routes_dict = RouteStop("0000", routes_list).get_route_dict()
    return stop_routes_dict


def get_lat_long_from_address(address):
    from geopy.geocoders import GoogleV3
    geolocator = GoogleV3()
    location = geolocator.geocode(address)
    return location.latitude, location.longitude
