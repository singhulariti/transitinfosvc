"""Utilities to work with Pyramid response objects in methos."""
from __future__ import print_function, unicode_literals, nested_scopes, generators, division,\
    absolute_import, with_statement, print_function, unicode_literals  # pragma: no cover

import simplejson
from pyramid import response


class TransInfoResponse(response.Response):
    """
    A convenience pyramid response specifically for transinfo so that we don't need to specify
    the content_type as text/plain all the time.
    """

    APPLICATION_JSON = str('application/json')

    def __init__(self, path, request=None, content_encoding=None, content_type=APPLICATION_JSON,
                 status_int=200):
        response.Response.__init__(self, path, content_type=content_type, request=request,
                                   content_encoding=content_encoding, status_int=status_int)


def get_json_response(original_response):
    return TransInfoResponse(simplejson.dumps(original_response))
