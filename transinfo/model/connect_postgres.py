import psycopg2
import getpass

def get_connection():
    try:
        #conn = psycopg2.connect(dbname='indianslang' user='indianslang' host='localhost' password='indianslang')
        #conn = psycopg2.connect(dbname='postgres' user='postgres' host='localhost' password='postgres')
        conn = psycopg2.connect('dbname=ssingh user={} host=localhost password=pipass'.format(getpass.getuser()))
        return conn
    except Exception, e:
        print('Failed to connect to database...: ' + str(e))