__author__ = 'ssingh'
import collections


class Route():
    def __init__(self, route_id, time_number, time_unit, current_location):
        self.bus_details = collections.OrderedDict()
        self.bus_details["route_id"] = route_id
        self.bus_details["time_number"] = time_number
        self.bus_details["time_unit"] = time_unit
        self.bus_details["current_location"] = current_location

    def get_bus_details(self):
        return self.bus_details