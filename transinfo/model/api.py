from transinfo.model.queries import get_closest_points, get_schedules, get_all_buses
from transinfo.util.transit_comm import get_lat_long_from_address
__author__ = 'ssingh'


def get_closest_stop(latitude, longitude):
    return get_closest_points(latitude, longitude, 1, 1)


def get_stop_by_address(address):
    latitude, longitude = get_lat_long_from_address(address)
    return get_closest_points(latitude, longitude, 1, 1)


def get_closest_stops(latitude, longitude):
    return get_closest_points(latitude, longitude)


def get_schedule_by_stop(stop_code, start_hour, start_min, year, month, day, route_code):
    if route_code is None or route_code == '':
        route_code = "%%"
    start = str(format(int(start_hour), '02')) + ":" + str(format(int(start_min), '02')) + ":00"
    end = str(format(int(start_hour) + 1, '02')) + ":00:00"
    month = str(format(int(month), '02'))
    day = str(format(int(day), '02'))
    date_string = str(year) + month + day
    return get_schedules("'" + stop_code + "'", "'" + start + "'", "'" + end + "'", "'" + route_code + "'",
                         "'" + date_string + "'")


def get_schedule(latitude, longitude, start_hour, start_min, year, month, day, route_code, address_or_id):
    try:
        if is_number(address_or_id):
            stop_code = address_or_id
        else:
            stop_id = get_stop_by_address(address_or_id)[0].get("stop_code")
            if stop_id:
                stop_code = stop_id
            else:
                stop_code = get_closest_stop(latitude, longitude)[0].get('stop_code')
    except:
        stop_code = get_closest_stop(latitude, longitude)[0].get('stop_code')

    return get_schedule_by_stop(stop_code, start_hour, start_min, year, month, day, route_code)


def get_all_routes():
    return get_all_buses()


def is_number(_id):
    try:
        int(_id)
    except:
        return False
    return True


if __name__ == "__main__":
    # print get_closest_stop(40.702487, -73.988888)
    print get_schedule(40.8444280, -74.11032011, "12", "00", "160")
