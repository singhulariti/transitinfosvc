from transinfo.model.connect_postgres import get_connection

__author__ = 'ssingh'


def get_closest_points(latitude, longitude, limit=3, min_distance=1):
    conn = get_connection()
    cur = conn.cursor()
    cur.execute("""select
                stop_code, stop_name
                ,((ACOS(SIN({0} * PI() / 180) * SIN("stop_lat" * PI() / 180) + COS({0} * PI() / 180) * COS("stop_lat" * PI() / 180) * COS(({1} - "stop_lon") * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS "distance"
                FROM stops
                WHERE
                (
                  "stop_lat" BETWEEN ({0} - {3}) AND ({0} + {3})
                  AND "stop_lon" BETWEEN ({1} - {3}) AND ({1} + {3})
                )
                ORDER BY "distance" ASC
                limit {2};""".format(latitude, longitude, limit, min_distance))
    rows = cur.fetchall()
    stops = []
    for row in rows:
        stops.append({"stop_code": row[0], "stop_name": row[1]})
    return stops


def get_schedules(stop_code, start_time, end_time, route_code, date_string):
    conn = get_connection()
    cur = conn.cursor()
    query = """SELECT s.stop_code as stop_code, r.route_short_name as route_short_name, 
                    to_char(st.arrival_time, 'HH24:MI:SS') as arrival_time, t.trip_headsign, s.stop_name as stop_name
                    FROM stop_times st, trips t, stops s, routes r, calendar_dates c
                    WHERE arrival_time > {0}
                    and s.stop_code = {1}
                    and r.route_short_name like {2}
                    and st.stop_id = s.stop_id
                    and st.trip_id = t.trip_id
                    and r.route_id = t.route_id
                    and c.service_id = t.service_id
                    and c.date = {3}
                    and c.exception_type = '1'
                    order by st.arrival_time
                    limit 10;""".format(start_time, stop_code, route_code, date_string)
    print query
    cur.execute(query)
    rows = cur.fetchall()
    routes = []
    for row in rows:
        routes.append({"stop_code": row[0], "route": row[1], "arrival_time": row[2], "headsign": row[3]})
    return routes


def get_all_buses():
    conn = get_connection()
    cur = conn.cursor()
    cur.execute("""SELECT r.route_short_name
                    FROM routes r;""")
    rows = cur.fetchall()
    routes = []
    for row in rows:
        routes.append(row[0])
    return routes