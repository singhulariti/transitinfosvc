__author__ = 'ssingh'
import collections


class Alert():
    def __init__(self, title, updated, description):
        self.alerts = collections.OrderedDict()
        self.alerts["title"] = title
        self.alerts["updated"] = updated
        self.alerts["description"] = description

    def get_alerts_list(self):
        return self.alerts