from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.include('pyramid_chameleon')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    _add_routes(config)
    config.scan()
    from wsgicors import CORS
    return CORS(config.make_wsgi_app(), headers="*", methods="*", maxage="180", origin="*")


def _add_routes(config):
    """
    map url's to route_names
    @param config: Configurator object
    """
    config.add_route('by_stop_code', '/by_stop_code/{stop_code}')
    config.add_route('by_stop_code_and_scheduled', 'by_stop_code_and_scheduled/{stop_code}')
    config.add_route('nearest', '/nearest')
    config.add_route('nearest_and_scheduled', 'nearest_and_scheduled')
    config.add_route('schedule', '/schedule')
    config.add_route('by_address', '/by_address')
    config.add_route('by_address_and_scheduled', '/by_address_and_scheduled')
    config.add_route('nearest_dummy', '/nearest_dummy')
    config.add_route('all_routes', '/all_routes')
    config.add_route('alerts', '/alerts')
