0. Create linux user "pi"
1. Install PostgreSQL
    1.1 sudo apt-get install postgresql python-psycopg2 libpq-dev
    1.2 sudo cp /etc/postgresql/9.6/main/pg_hba.conf /etc/postgresql/9.6/main/pg_hba.conf.bak
    1.3 Authentication
        1.3.1 sudo vim /etc/postgresql/9.6/main/pg_hba.conf
        1.3.2 Paste "host     all     all     192.168.0.0/24     md5" in the end
    1.4 sudo -u postgres createuser --interactive
        Enter name of role to add: pi
        Shall the new role be a superuser? (y/n) y
    1.5 sudo -u pi createdb ssingh
    1.6 psql -d ssingh -a -f gtfsschema.sql
    1.7 mkdir ~/gtfsfiles
    1.8 cd ~/gtfsfiles && wget https://transitfeeds.com/p/nj-transit/409/latest/download bus_data.zip
    1.9 ./reload_data.sh pi bus_data.zip
    2.0 apt-get install supervisor
        2.0.1 sudo cp supervisor_scripts/transitinfo.conf /etc/supervisor/conf.d/
        2.0.2 chmod +x supervisor_scripts/supervisor_script.sh
        2.0.3 sudo supervisorctl reread
        2.0.4 sudo supervisorctl update
        2.0.5 sudo service supervisor restart

