#!/usr/bin/env bash
echo "Deleting files..."
rm /home/$1/gtfsfiles/*.txt
echo "Unzipping files for $2"
unzip /home/$1/gtfsfiles/$2 -d /home/$1/gtfsfiles/

echo "Load data for stops"
psql -U $1 -d ssingh -c 'delete from stops;'
psql -U $1 -d ssingh -c "COPY stops (stop_id,stop_code,stop_name,stop_desc,stop_lat,stop_lon,zone_id) FROM '/home/$1/gtfsfiles/stops.txt' CSV HEADER";

echo "Load data for trips"
psql -U $1 -d ssingh -c 'delete from trips;'
psql -U $1 -d ssingh -c "COPY trips (route_id,service_id,trip_id,trip_headsign,direction_id,block_id,shape_id) FROM '/home/$1/gtfsfiles/trips.txt' CSV HEADER;"

echo "Load data for routes"
psql -U $1 -d ssingh -c 'delete from routes;'
psql -U $1 -d ssingh -c "COPY routes (route_id,agency_id,route_short_name,route_long_name,route_type,route_url,route_color) FROM '/home/$1/gtfsfiles/routes.txt' CSV HEADER;"

echo "Load data for calendar_dates"
psql -U $1 -d ssingh -c 'delete from calendar_dates;'
psql -U $1 -d ssingh -c "COPY calendar_dates (service_id,date,exception_type) FROM '/home/$1/gtfsfiles/calendar_dates.txt' CSV HEADER;"

echo "Load data for stop_times"
psql -U $1 -d ssingh -c 'delete from stop_times;'
psql -U $1 -d ssingh -c "COPY stop_times (trip_id,arrival_time,departure_time,stop_id,stop_sequence,pickup_type,drop_off_type,shape_dist_traveled) FROM '/home/$1/gtfsfiles/stop_times.txt' CSV HEADER;"
